#! /usr/bin/env python3

from flask import Flask, render_template
from db import DB

app = Flask(__name__)
app.debug = True
#~ taches={}
#~ taches["Paul"]=["nap","nothing","testing"]
#~ taches["Jack"]=["nothing","testing","git"]

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    db=DB()
    todo=[]
    for n,t in db.get(name):
        todo.append(t)
    return render_template(
        "user.html",
        name=name,
        todo=todo)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
